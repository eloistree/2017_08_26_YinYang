﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
public class moveCharacters : MonoBehaviour {

	public AudioSource scream;

	public bool startGameOk = false;

	public Sprite normalFace;
	public Sprite angryFace;
	public Vector3 startPos = new Vector3 (14.76f, -5.35f, 0);
	public Vector3 endPos = new Vector3 (5.89f, -5.35f, 0);

	public GameObject Arrow;
	public Sprite Up;
	public Sprite Down;

	public string key;

	float lerpTime = 2f;
	float currentLerpTime;

	public GameObject bg;

	private Renderer rend;
	public Color ColorRed = new Color32( 0xd4, 0x49, 0x49, 0xFF );
	public Color ColorNeutral = new Color32( 0xFF, 0xFF, 0xFF, 0xFF );

	public bool shakeAnim = true;


	//image shaking
	private float titleXmin, titleXmax, titleYmin, titleYmax;
	public float shakeForce = 0.05f;



    public int _playerIndex;

	// Use this for initialization
	void Start () {

		rend = bg.GetComponent<Renderer>();
		rend.material.shader = Shader.Find("GUI/Text Shader");

		titleXmin = endPos.x-shakeForce;
		titleXmax = endPos.x+shakeForce;
		titleYmin = endPos.y-shakeForce;
		titleYmax = endPos.y+shakeForce;

	}

	// Update is called once per frame
	void Update () {


		currentLerpTime += Time.deltaTime;
		if (currentLerpTime > lerpTime) {
			currentLerpTime = lerpTime;
		}

		//lerp!
		float t = currentLerpTime / lerpTime;
		//t = t * t * (3f - 2f * t);
		t = Mathf.SmoothStep(0,1,t);
		if(t<1)transform.position = Vector3.Lerp(startPos, endPos, t);


		//P1
		var sp_button = Arrow.GetComponent<SpriteRenderer>();
		var sp = GetComponent<SpriteRenderer>();

        bool rewiredRead = ReInput.players.GetPlayer(_playerIndex).GetAnyButton();
       
		if (Input.GetKey(key) || startGameOk || rewiredRead) {

			rend.material.SetColor("_Color", ColorRed);
			sp.sprite = angryFace;
			sp_button.sprite = Down;

			//shake animation
			float shakeH = Random.Range(titleXmin,titleXmax);
			float shakeV = Random.Range(titleYmin,titleYmax);
			if (shakeAnim) transform.position = new Vector3 (shakeH,shakeV, transform.position.z);

			if(!scream.isPlaying && shakeAnim)scream.Play();
		} else {
			rend.material.SetColor("_Color", ColorNeutral);
			sp.sprite = normalFace;
			sp_button.sprite = Up;
			scream.Stop();
		}

	}
}
