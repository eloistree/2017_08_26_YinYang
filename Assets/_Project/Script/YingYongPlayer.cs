﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Events;

public class YingYongPlayer : MonoBehaviour {


    public YingYong _team;
    public Rewired.Player _player;
    public int _playerIndex = 0;

    public Rigidbody _rigBody;
    [SerializeField]
    private float _forceSpeed=1f;
    public void SetSpeed(float speed) {
        if (speed < 0f)
            speed = 0;
        _forceSpeed = speed;
    }



    public float _cooldownFiring = 1f;
    public float _cooldownState = 0f;


    public ForceMode _forceModeUsed;

    public Transform _whereToCreateMob;


    [Header("Events")]
    public UnityEvent onPlayerKilled;
    public UnityEvent onPlayerFired;

    [Header("Debug")]
    public Vector2 _joyDirection;

    void Awake() {

        TeamAffected[] teamAffected = GetComponentsInChildren<TeamAffected>();
        for (int i = 0; i < teamAffected.Length; i++)
        {
            teamAffected[i].SetTeam(_team);

        }
    }

	void Start () {
        _player = ReInput.players.GetPlayer(_playerIndex);

        
    }
    
    
	void Update ()
    {
        _joyDirection.x = _player.GetAxis("MoveHorizontal");
        _joyDirection.y = _player.GetAxis("MoveVertical");
        _rigBody.AddForce(new Vector3(_joyDirection.x, 0, _joyDirection.y)* _forceSpeed*Time.deltaTime , _forceModeUsed);


        if (_cooldownState > 0f)
            _cooldownState -= Time.deltaTime;

        if (_cooldownState < 0f) {
            _cooldownState = 0f;

        }
        if (_cooldownState==0f) {
            bool spawning = false;
            if (_player.GetButtonDown("FireMobType1"))
            {
                CreateMob("L");
                spawning = true;
            }

            if (_player.GetButtonDown("FireMobType2"))
            {

                CreateMob("V");
                spawning = true;

            }
            if (_player.GetButtonDown("FireMobType3"))
            {

                CreateMob("O");
                spawning = true;
            }
            if (_player.GetButtonDown("FireMobType4"))
            {
                CreateMob("Random");
                spawning = true;
            }

            if (spawning) {
                _cooldownState = _cooldownFiring;
            }
        }





    }
    public void CreateMob(string id)
    {

        MobSpawning.InstanceInScene.CreateMob(id, _whereToCreateMob.position, _whereToCreateMob.rotation, _team);
    }


}

public enum YingYong : int { Black, White }

public interface TeamAffected {

    void SetTeam(YingYong team);
    YingYong GetTeam();
    bool IsTeamBeenDefined();
}