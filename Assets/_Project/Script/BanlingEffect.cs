﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BanlingEffect : MonoBehaviour, TeamAffected
{


    public Transform _whereToApplyEffect;
    public GameObject [] _objectsToDestroy;
    public GameObject _explosionPrefab;

    public UnityEvent onExplode;
    public YingYong _team;
    public bool _teamDefined;
    
    public void Update() {

        //DEBUG REMOVE LATER
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.E))
            Explose();
    }
	
	public void Explose () {

        Vector3 position = _whereToApplyEffect.position;
        Quaternion rotation = _whereToApplyEffect.rotation ;
        GameObject explosition = Instantiate(_explosionPrefab, position, rotation);


        PrintImageToTexture[] printers = explosition.GetComponentsInChildren<PrintImageToTexture>();
        for(int i = 0 ; i< printers.Length ; i++)
        {
            printers[i].Print(_team);
        }

        onExplode.Invoke();

        for (int i = 0; i < _objectsToDestroy.Length; i++)
        {
            Destroy(_objectsToDestroy[i]);
        }

    }
    
    public void SetTeam(YingYong team)
    {
        _teamDefined = true;
        _team = team;
    }

    public YingYong GetTeam()
    {
        return _team;
    }

    public bool IsTeamBeenDefined()
    {
        return _teamDefined;
    }


}
