﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTriggerZone : MonoBehaviour {
    
    public LayerMask _interctWith;
    public void OnTriggerEnter(Collider collision)
    {
        GameObject affected = collision.attachedRigidbody.gameObject;
        if (!(_interctWith == (_interctWith | (1 << affected.layer))))
            return;
            
        IKillabe killable = affected.GetComponent<IKillabe>();
        if (killable != null) {
            killable.KillIt();
        }
        
    }
}


