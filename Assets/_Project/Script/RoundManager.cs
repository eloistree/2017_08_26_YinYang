﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RoundManager : MonoBehaviour {

    public UnityEvent onRoundStart;
    public PlayerDeathEvent onPlayerDeath;
    public float _durationBetweenCheck=0.1f;


    public YingYongPlayer _black;
    public YingYongPlayer _white;

    public bool _endGameHasBeenActivated;


    

    IEnumerator Start () {
        NotifyPlayerDeath();
        while (!_endGameHasBeenActivated) {
            yield return new WaitForSeconds(_durationBetweenCheck);
            CheckForDeathPlayer();
    	}
    }

    private void NotifyPlayerDeath()
    {
        onRoundStart.Invoke();
    }

    private void CheckForDeathPlayer()
    {
        if (_black == null || !_black.gameObject.activeInHierarchy)
            NotifyPlayerDeath(YingYong.Black);
        else if (_white == null || !_white.gameObject.activeInHierarchy)
            NotifyPlayerDeath(YingYong.White);
    }

    private void NotifyPlayerDeath(YingYong team)
    {
        _endGameHasBeenActivated = true;
        onPlayerDeath.Invoke(team);
    }
    
    [System.Serializable]
    public class PlayerDeathEvent : UnityEvent<YingYong>
    {

    }
}
