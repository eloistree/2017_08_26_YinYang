﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateInDirection : MonoBehaviour {

    public Vector3 _lastPosition;
    public Vector3 _newPosition;
    public float _minDistance = 0.03f;
    public float _rotationFactor = 1f;
    public Rewired.Player _player;
    public int _playerIndex;
    [Header("Debug")]
    public Vector3 _direction;
    public float _lastAngle;


	void Awake () {

        _player = ReInput.players.GetPlayer(_playerIndex);
        //while (true) {
        //    yield return new WaitForSeconds(0.2f);

        //    _newPosition = new Vector3(transform.position.x, 0, transform.position.z);
        //    if (Vector3.Distance(_lastPosition, _newPosition) > _minDistance) {
        //        _direction = _newPosition - _lastPosition;
        //        _lastPosition = _newPosition;

        //    }
        //}

    }

    public void Update() {


        _direction.x = _player.GetAxis("MoveHorizontal");
        _direction.z = _player.GetAxis("MoveVertical");

        if (_direction.magnitude > 0.1f) { 
            float angle = Mathf.Rad2Deg * Mathf.Atan2(-_direction.z, _direction.x) + 90f;

            //float angle = Mathf.Rad2Deg * Mathf.Atan2(-, _direction.x) + 90f;
            //float angle = Mathf.Rad2Deg * Mathf.Atan2(-_direction.z, _direction.x) + 90f;

            float wantedAngle = Mathf.MoveTowards(_lastAngle, angle, Time.deltaTime * 2);
            transform.eulerAngles = new Vector3(0, wantedAngle, 0);
            _lastAngle = angle;
        }


    }
}
