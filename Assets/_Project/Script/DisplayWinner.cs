﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayWinner : MonoBehaviour {


    [SerializeField]
    public UIOfWinner _black;

    [SerializeField]
    public UIOfWinner _white;

    [System.Serializable]
    public struct UIOfWinner {
        public GameObject[] _toActivate;
        public Text _txtWinner;
        public Image _winnerAvatar;
        public Image _winnerAnnoncement;
        public AudioClip _winSound;
    }


    public void SetWinnerAs(YingYong winner) {
        if (winner == YingYong.Black)
            SetAsBlackPlayerWin();
        if (winner == YingYong.White)
            SetAsWhitePlayerWin();
    }

    public void SetAsBlackPlayerWin() {
        SetWith(_black);
    }
    public void SetAsWhitePlayerWin() {
        SetWith(_white);
    }

    public void SetWith(UIOfWinner winner)
    {
        for (int i = 0; i < winner._toActivate.Length; i++)
        {
            winner._toActivate[i].SetActive(true);
        }

        if(winner._txtWinner)
            winner._txtWinner.gameObject.SetActive(true);
        if (winner._winnerAvatar)
            winner._winnerAvatar.gameObject.SetActive(true);
        if (winner._winnerAnnoncement)
            winner._winnerAnnoncement.gameObject.SetActive(true);

    }

}
