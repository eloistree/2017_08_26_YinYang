﻿Shader "Hidden/RenderEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BlackWhiteBuffer("blackwhite", 2D) = "white" {}
		_BloodBuffer("blood",2D) = "clear" {}
		_PropagateBuffer("propagateBuffer",2D) = "clear" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _BlackWhiteBuffer;
			sampler2D _BloodBuffer;
			sampler2D _PropagateBuffer;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				float4 blood = tex2D(_BloodBuffer,i.uv);

				
				col = lerp(col,1 - col, tex2D(_BlackWhiteBuffer,i.uv).r);

				col = lerp(col,blood,blood.a);

				float4 propagator = tex2D(_PropagateBuffer, i.uv);
				if(propagator.r> 0.01)
				{
					col = float4((i.uv.y * 500) % 2 < 1 ? 0 : 0.5, propagator.r, (i.uv.x * 1000) % 2 < 1 ? 0 : 1, 1);
				}
				else if (propagator.r > 0.51) {
					col = float4((i.uv.y * 500) % 2 < 1 ? 0 : 0.5, propagator.r*2, (i.uv.x * 1000) % 2 < 1 ? 0 : 1, 1);
				}

				return col;
			}
			ENDCG
		}
	}
}
