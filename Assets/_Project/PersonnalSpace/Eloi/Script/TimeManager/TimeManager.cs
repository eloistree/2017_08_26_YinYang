﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeManager : MonoBehaviour {

    [SerializeField]
    private bool _pause;
    public void SwitchPause() {
        _pause = !_pause;
        onPause.Invoke(_pause);
    }

    public class PauseEvent : UnityEvent<bool> { }
    public PauseEvent onPause;




}
